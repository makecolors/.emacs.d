(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)

;; インストールするパッケージ
(defvar my/favorite-packages
  '(
    init-loader
    auto-complete
    hlinum
    recentf
    php-mode
  ))

;; my/favorite-packagesからインストールしていないパッケージをインストール
(dolist (package my/favorite-packages)
  (unless (package-installed-p package)
        (package-install package)))
