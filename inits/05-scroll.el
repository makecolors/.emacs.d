;; スクロールした際のカーソルの移動行数
(setq scroll-conservatively 1)

;; スクロール開始のマージンの行数
(setq scroll-margin 1)

;; 1 画面スクロール時に重複させる行数
(setq next-screen-context-lines 1)

;; 1 画面スクロール時にカーソルの画面上の位置をなるべく変えない
(setq scroll-preserve-screen-position t)

;; マウスホイールによるスクロール時の行数
;;   Shift 少なめ、 Ctrl 多めに移動
(setq mouse-wheel-scroll-amount
      '(5                              ; 通常
        ((shift) . 1)                   ; Shift
        ((control) . 40)                ; Ctrl
))

