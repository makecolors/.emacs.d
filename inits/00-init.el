;; バックアップファイルを作成しない
(setq make-backup-files nil)

;; オートセーブファイルを作らない
(setq auto-save-default nil)

;; タブの代わりに半角スペースを使う
(setq-default tab-width 4 indent-tabs-mode nil)

;;; 自動インデントをしない
(electric-indent-mode 0)

;;; emacsのヘッダーを非表示にする
(menu-bar-mode 0)

;; 括弧の範囲内を強調表示
(show-paren-mode t)
(setq show-paren-delay 0)
(setq show-paren-style 'expression)

;; 選択領域の色
(set-face-background 'region "#F77")

;; 'yes or no'の問いを'y or n'に変換する
(fset 'yes-or-no-p 'y-or-n-p))

