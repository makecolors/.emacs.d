(require 'hlinum)

;; 前景色を黒,背景色を赤にする.
(custom-set-faces '(linum-highlight-face ((t (:foreground "black" :background "red")))))

;; 常に linum-mode を有効
(custom-set-variables '(global-linum-mode t))

