(require 'auto-complete)
(require 'auto-complete-config)
(global-auto-complete-mode t)
(setq ac-comphist-file "~/.emacs.d/cache/auto-complete/ac-comphist.dat")
