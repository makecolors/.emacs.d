;; パッケージ管理ソフトであるpackage.elを先に読み込む
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(package-initialize)

;; init-loaderは必ず読み込む設定
(require 'init-loader)
(setq init-loader-show-log-after-init nil)
;; If you omit arguments, then `init-loader-directory' is used
(init-loader-load "~/.emacs.d/inits")

